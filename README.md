# README #

A programming challenge from Favro (the assigner) to Helena Udd (the assignee) to test my programming skills as an assessment of a potential employment.

## How to play ##
As setting up a database and domain is out of the scope of this project, one has to install the Meteor framework in order to run this application. Normally, the installed Node.js modules are not committed to a repository as they are installed locally on each machine, but for ease of access to this server, I have made an exception and pushed the `node_modules` folder to the repository for others to be able to run the server right away.

1. Clone the repository's master branch.
2. Follow the instructions on Meteor's website to install Meteor and Node.js if not installed already.
3. In your local clone of the repository, in the /gomoku/ folder (which contains the .meteor folder), run the command `meteor` to start the Gomoku server on your local host.
4. In your browser, go to `http://localhost:3000` to connect to the game. At least two different users need to be connected to play. Other clients on the same network can also connect through the local IP to your machine, like `http://192.168.1.10:3000` as an example.
 
### Rules ###
* You have to be signed in in order to play, otherwise you will be a spectator.
* If there are already two signed in users on the page, they are the players and any other users will be spectators.
* If one user signs out, another user can sign in (or refresh the page if already signed in) to take their place.
* Everyone can start a new game, even spectators. Use with caution!
* If there are user and/or player problems, restart the server to reset the players.
* The only game rules that are implemented are that at least 5 markers are needed in a row to win.

## Assignment ##

### Programming Challenge ###

Implement a Gomoku game (“five in a row”) using the Meteor framework.

Read more about Gomoku on Wikipedia: https://en.wikipedia.org/wiki/Gomoku

Read more about the Meteor framework: https://www.meteor.com/

#### Primary Objectives ####

* Present a playing board, and let players place “X” or “O” on the board
* Let two players play against each other in their own browsers

#### Bonus Objectives ####

* Strictly enforce game rules
* Check the victory condition and display a message when it’s reached
* Support multiple parallel game sessions
* Suggest a reasonable move for a player on request

It is not required to implement everything listed above. Use your judgement on exactly how much you want to implement to show your skills.


## Implementation ##

### Project scope ###

The challenge was issued on November 1 2017, and is to be completed by the week after (November 6-10 2017).

### Risks and challenges ####

* The assignee has no previous experience of JavaScript. Impact: low. Motivation: Learning new languages is a rather smooth and easy task, but requires some research.

* The assignee has no previous experience with browser implementation, HTML and CSS. Impact: medium. Motivation: The usage of the Meteor framework reduces the impact greatly, as it provides much of the functionality. Research is still needed.

### Rules and guidelines

* Code convention: W3Schools. https://www.w3schools.com/js/js_conventions.asp

### Time spent ###
Total time spent on project: 23 hours.

#### Pre-implenentation research and tutorial ####
6 hours.

#### Primary objectives ####
12.5 hours.

#### Bonus objectives ####

##### Enforce game rules #####
Partly implemented. Some basic rules are enforced, like taking turns, but no other balancing rules are implemented.

##### Check victory condition #####
2 hours.

##### Support multiple parallel game sessions #####
Not implemented.

##### Suggest a reasonable move for a player on request #####
Not implemented.

#### Extras ####

##### Status texts #####
0.5 hours.

##### Start new game from client #####
0.5 hours.

##### Deployment and testing #####
1.5 hours.