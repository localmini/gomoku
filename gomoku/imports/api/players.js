import { Mongo } from 'meteor/mongo';
import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';

export const Players = new Mongo.Collection('players');

if (Meteor.isServer) {
    Meteor.publish('players', function playersPublication() {
        return Players.find();
    });
}

Meteor.methods({
    'players.insert'(id, player) {
        if (!Meteor.isServer) {
            throw new Meteor.Error('not-authorized');
        }

        check(id, String);
        check(player, Number);

        Players.insert({
            _id: id,
            player: player
        });
    },

    'players.upsert'(id, player) {
        if (!Meteor.isServer) {
            throw new Meteor.Error('not-authorized');
        }

        check(id, String);
        check(player, Number);

        Players.upsert({
            _id: id
        }, {
            $set: {
                player: player
            }
        });
    },

    'players.update'(id, player) {
        if (!Meteor.isServer) {
            throw new Meteor.Error('not-authorized');
        }

        check(id, String);
        check(player, Number);

        Players.update(id, {
            $set: { player: player },
        });
    },

    'players.remove'(id) {
        if (!Meteor.isServer) {
            throw new Meteor.Error('not-authorized');
        }

        check(id, String);

        Players.remove(id);
    },

    'players.removeAll'() {
        if (!Meteor.isServer) {
            throw new Meteor.Error('not-authorized');
        }

        Players.remove({});
    }
});