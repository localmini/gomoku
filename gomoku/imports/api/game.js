import { Meteor } from 'meteor/meteor';

import { Squares } from './squares.js';
import { Players } from './players.js';

Meteor.methods({
    'game.restart'() {
        if (Meteor.isServer) {
            Meteor.call("players.upsert", "winner", 0);
            Meteor.call("players.upsert", "current", 1);

            var rowIndex = 0;
            var columnIndex = 0;
            var nrOfRows = 15;
            var nrOfColumns = 15;

            for (rowIndex = 0; rowIndex < nrOfRows; rowIndex++) {
                column = [];
                for (columnIndex = 0; columnIndex < nrOfColumns; columnIndex++) {
                    var id = rowIndex * nrOfColumns + columnIndex;
                    id++;  //ID 0 is not supported
                    Meteor.call("squares.upsert", id, "");
                }
            }
        }
    },

    'game.setStatus'(gameStatus) {
        if (Meteor.isClient) {
            var gameStatusElement = document.getElementsByClassName('game-status');
            if (typeof gameStatusElement !== "undefined" &&
                gameStatusElement !== null && gameStatusElement.length > 0) {

                gameStatusElement[0].innerHTML = gameStatus;
            }
        }
    }
});