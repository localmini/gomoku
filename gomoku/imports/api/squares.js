import { Mongo } from 'meteor/mongo';
import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';

import { Players } from './players.js';

export const Squares = new Mongo.Collection('squares');

if (Meteor.isServer) {
    Meteor.publish('squares', function squaresPublication() {
        var cursor = Squares.find();
        cursor.observeChanges({
            changed: function(id, object) {
                var currentPlayerDatabase = Players.findOne({_id: "current"});
                var currentPlayer = currentPlayerDatabase.player;

                if (currentPlayer === 1 && object.value === "X") {
                    //If player 1 made their move, check if they won.
                    //Then switch to player 2.

                    var win = calculateWinner({_id: id, value:object.value});
                    if (win === true) {
                        Meteor.call("players.upsert", "winner", 1);
                    }

                    Meteor.call("players.update", "current", 2);
                }
                else if (currentPlayer === 2 && object.value === "O") {
                    //If player 2 made their move, check if they won.
                    //Then switch to player 1.

                    var win = calculateWinner({_id: id, value:object.value});
                    if (win === true) {
                        Meteor.call("players.upsert", "winner", 2);
                    }

                    Meteor.call("players.update", "current", 1);
                }
            }
        });
        return cursor;
    });
}

function calculateWinner(square) {
    var nrOfMarksWestEast = 0;
    var nrOfMarksNorthwestSoutheast = 0;
    var nrOfMarksNorthSouth = 0;
    var nrOfMarksNortheastSouthwest = 0;
    const requiredMarkers = 4;

    //Calculate the amount of markers of the given type that are found in each
    //direction. If a total of at least 4 markers are found in any direction
    //(where an example direction is the west-east direction), the player
    //has won. This is called free-style Gomoku.
    //5 in a row are required, but the given square is not counted, hence the 4.

    //West
    nrOfMarksWestEast += calculateMarkersInDirection(square,
    function(id) {
        return id - 1;
    });
    if (nrOfMarksWestEast >= requiredMarkers) {
        return true;
    }

    //East
    nrOfMarksWestEast += calculateMarkersInDirection(square,
    function(id) {
        return id + 1;
    });
    if (nrOfMarksWestEast >= requiredMarkers) {
        return true;
    }


    //Northwest
    nrOfMarksNorthwestSoutheast += calculateMarkersInDirection(square,
    function(id) {
        return id - 16;
    });
    if (nrOfMarksNorthwestSoutheast >= requiredMarkers) {
        return true;
    }

    //Southeast
    nrOfMarksNorthwestSoutheast += calculateMarkersInDirection(square,
    function(id) {
        return id + 16;
    });
    if (nrOfMarksNorthwestSoutheast >= requiredMarkers) {
        return true;
    }


    //North
    nrOfMarksNorthSouth += calculateMarkersInDirection(square,
    function(id) {
        return id - 15;
    });
    if (nrOfMarksNorthSouth >= requiredMarkers) {
        return true;
    }

    //South
    nrOfMarksNorthSouth += calculateMarkersInDirection(square,
    function(id) {
        return id + 15;
    });
    if (nrOfMarksNorthSouth >= requiredMarkers) {
        return true;
    }


    //Northeast
    nrOfMarksNortheastSouthwest += calculateMarkersInDirection(square,
    function(id) {
        return id - 14;
    });
    if (nrOfMarksNortheastSouthwest >= requiredMarkers) {
        return true;
    }

    //Southwest
    nrOfMarksNortheastSouthwest += calculateMarkersInDirection(square,
    function(id) {
        return id + 14;
    });
    if (nrOfMarksNortheastSouthwest >= requiredMarkers) {
        return true;
    }


    return false;
}

function calculateMarkersInDirection(square, calculateNextId) {
    var marker = square.value;
    var markerFound = true;
    var squareId = square._id;
    var nrOfMarksInARow = 0;

    while (markerFound && nrOfMarksInARow < 4) {
        var nextId = calculateNextId(squareId);
        var nextSquare = Squares.findOne({_id: nextId});

        if (typeof nextSquare !== "undefined" && nextSquare.value === marker) {
            markerFound = true;
            squareId = nextId;
            nrOfMarksInARow++;
        } else {
            markerFound = false;
        }
    }

    return nrOfMarksInARow;
}

Meteor.methods({
    'squares.insert'(id, value) {
        if (!Meteor.isServer) {
            throw new Meteor.Error('not-authorized');
        }

        check(id, Number);
        check(value, String);

        Squares.insert({
          id,
          value
        });
    },

    'squares.upsert'(id, value) {
        if (!Meteor.isServer) {
            throw new Meteor.Error('not-authorized');
        }

        check(id, Number);
        check(value, String);

        Squares.upsert({
            _id: id
        }, {
            $set: {
                value: value
            }
        });
    },

    'squares.update'(id, value) {
        if (Meteor.isClient && !Meteor.userId()) {
            throw new Meteor.Error('not-authorized');
        }

        check(id, Number);
        check(value, String);

        Squares.update(id, {
            $set: { value: value },
        });
    }
});