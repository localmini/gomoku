import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { createContainer } from 'meteor/react-meteor-data';
import { Squares } from '../api/squares.js';
import Square from './Square.jsx';
import { Players } from '../api/players.js';

class Board extends Component {
    renderSquares(startIndex, endIndex) {
        if (startIndex === undefined)
            startIndex = 0;
        if (endIndex === undefined)
            endIndex = 1;
        if (startIndex > endIndex)
            alert("Start index has to be less than end index!");

        var squares = [];
        var square;

        for (var i = startIndex; i < endIndex; i++) {
            square = this.props.squares[i];
            if (typeof square !== "undefined") {
                squares.push(<Square key = {square._id} square = {square}
                    onClick = {mouseEvent => this.onSquareClick(mouseEvent)}/>);
            }
        }

        return squares;
    }

    render() {
        var rows = [];
        var nrOfRows = 15;
        var nrOfColumns = 15;
        var rowIndex = 0;

        for (rowIndex = 0; rowIndex < nrOfRows; rowIndex++) {
            var startIndex = rowIndex * nrOfColumns;
            var endIndex = startIndex + nrOfColumns - 1;

            rows.push(
                //The squares use positive keys from 0 and up to an
                //unknown number, so the rows use negative keys from -1 and down
                //to make them unique.
                <div className = "boardRow" key = {(rowIndex + 1) * -1}>
                    {this.renderSquares(startIndex, endIndex)}
                </div>
            );
        }

        return (
            <div className = "board">
                {rows}
            </div>
        );
    }

    onSquareClick(mouseEvent) {
        //Do nothing if we aren't logged in.
        if (typeof Meteor.user() === "undefined" && Meteor.user() === null) {
            return;
        }

        var current = Players.findOne({_id: "current"});
        var winner = Players.findOne({_id: "winner"});
        var me = Players.findOne({_id: Meteor.userId()});

        //Do nothing if someone has won already.
        if (typeof winner !== "undefined" && winner.player !== 0) {
            return;
        }

        //Only do something if we are a player and if it's our turn.
        if (typeof me === "undefined" || typeof current === "undefined" ||
            current.player !== me.player) {
                return;
        }

        var clickedSquareIdString = mouseEvent.target.getAttribute("data-id");
        var clickedSquareId = parseInt(clickedSquareIdString);
        var clickedSquare = Squares.findOne({_id: clickedSquareId})
        var marker = me.player === 1 ? "X" : "O";

        //Only mark the square if it's not already taken.
        if (typeof clickedSquare !== "undefined" &&
            clickedSquare.value === "") {

            Meteor.call("squares.update", clickedSquareId, marker);
        }
    }
}

Board.propTypes = {
    //This is a two-dimensional array, with rows in the first dimension
    //and the columns (the actual squares) in the second dimension.
    squares: PropTypes.array.isRequired
};

export default createContainer(() => {
    Meteor.subscribe('squares');
    const handle = Meteor.subscribe('players');
    Tracker.autorun(() => {
        const isReady = handle.ready();
        if (isReady) {
            var winner = Players.findOne({_id: "winner"});

            if (typeof winner !== "undefined") {
                var me = Players.findOne({_id: Meteor.userId()});

                if (winner.player === 0) {
                    var current = Players.findOne({_id: "current"});

                    if (typeof current !== "undefined") {
                        if (typeof me !== "undefined") {
                            if (current.player === me.player) {
                                Meteor.call('game.setStatus', "Your turn.");
                            } else {
                                Meteor.call('game.setStatus', "Waiting for " +
                                    "the other player to make their move.");
                            }
                        } else {
                            Meteor.call('game.setStatus', "Spectating. It's " +
                                "player " + current.player + "'s turn.");
                        }
                    }
                } else {
                    if (typeof me !== "undefined") {
                        if (winner.player === me.player) {
                            Meteor.call('game.setStatus', "You won!");
                        } else {
                            Meteor.call('game.setStatus', "Your opponent won!");
                        }
                    } else {
                        Meteor.call('game.setStatus', "Player " + winner.player
                            + " has won!");
                    }
                }
            }
        }
    });


    return {
        squares: Squares.find({}).fetch()
    };
}, Board);