import React, { Component } from 'react';
import { Squares } from '../api/squares.js';
import { Meteor } from 'meteor/meteor';

import Board from './Board.jsx';
import AccountsUIWrapper from './AccountsUIWrapper.jsx';
import '../api/game.js';

export default class Game extends Component {
    render() {
        return (
          <div className = "game">
            <header>
              <h1>Gomoku</h1>
            </header>

            <AccountsUIWrapper />

            <button className="new-game" onClick={() => this.restartGame()}>
                New game
            </button>

            <div className="game-status">
            </div>

            <div className = "game-board">
              <Board squares = {[]}/>
            </div>
          </div>
        );
    }

    restartGame() {
        if (confirm("Are you sure you want to start a new game? The ongoing " +
            "game will end.") === true) {

            Meteor.call('game.restart');
        }
    }
}