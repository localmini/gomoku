import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class Square extends Component {
    render() {
        return (
            <button className = "square" data-id = {this.props.square._id}
                onClick = {this.props.onClick}>
                {this.props.square.value}
            </button>
        );
    }
}

Square.propTypes = {
    square: PropTypes.object.isRequired,
    onClick: PropTypes.func.isRequired
};