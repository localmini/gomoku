import { Meteor } from 'meteor/meteor';
import '../imports/api/squares.js';
import { Squares } from '../imports/api/squares.js';
import { Players } from '../imports/api/players.js';
import '../imports/api/game.js';

Meteor.startup(() => {
    // code to run on server at startup
    Meteor.call("players.removeAll");
    Meteor.call("game.restart");
});

Accounts.onLogin(function(user){
    var existing = Players.findOne(user.user._id);
    var currentPlayers = [];
    var playerIndex = 0;

    //If the user already is a player, remove it.
    if (typeof existing !== "undefined") {
        Meteor.call("players.remove", user.user._id);
    }

    currentPlayers = Players.find({$and: [{_id: {$ne:"current"}}, {_id: {$ne:"winner"}}]}).fetch();
    playerIndex = 0;

    if (currentPlayers.length === 0) {
        //If there are no previous players, this user is player one.
        playerIndex = 1;
    } else if (currentPlayers.length === 1) {
        //If there is one previous player, this user will take up the
        //player index that is not previously in use.
        playerIndex = currentPlayers[0].player === 1 ? 2 : 1;
    } else {
        //If there already 2 active players, no more can be added.
        console.log(user.user.username + " logged in. " +
            "There are already 2 players active, " +
            user.user.username + " will not be counted as a player.");
        return;
    }

    //Insert the user as a player with the given player index.
    Meteor.call("players.insert", user.user._id, playerIndex);

    console.log(user.user.username + " logged in and was assigned as player " +
        playerIndex + ".");
});

Accounts.onLogout(function(user){
    console.log(user.user.username + " logged out.");
    Meteor.call("players.remove", user.user._id);
});