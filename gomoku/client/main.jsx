import React from 'react';
import { Meteor } from 'meteor/meteor';
import { render } from 'react-dom';

import '../imports/startup/accounts-config.js';
import Game from '../imports/ui/Game.jsx';

Meteor.startup(() => {
    render(<Game />, document.getElementById('render-target'));
});